# -*- coding: utf-8 -*-
# Copyright (C) 2014 Sebastien Gedeon, IUT d'Orl�ans
#==============================================================================

from .event import Event3

class ExplodeWithEvent(Event3):
    NAME = "explode-with"

    def perform(self):
        if not self.object.has_prop("breakable") and not self.object2.has_prop("explosif"):
            self.fail()
            return self.inform("explode-with.failed")
        self.inform("explode-with")