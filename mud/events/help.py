# -*- coding: utf-8 -*-
# Copyright (C) 2014 Sebastien Gedeon, IUT d'Orl�ans
#==============================================================================

from .event import Event1

class HelpEvent(Event1):
    NAME = "help"
    
    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        self.inform("help")