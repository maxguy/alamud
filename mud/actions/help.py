# -*- coding: utf-8 -*-
# Copyright (C) 2014 Sebastien Gedeon, IUT d'Orl�ans
#==============================================================================

from .action import Action1
from mud.events import HelpEvent

class HelpAction(Action1):
    EVENT = HelpEvent
    ACTION = "help"
